# Listening and Active Communication

# 1. Active Listening
### Question 1
#### What are the steps/strategies to do Active Listening? (Minimum 6 points)

* Face the speaker and have eye contact. 
* “Listen” to non-verbal cues too. 
* Don't interrupt. 
* Listen without judging, or jumping to conclusions.
* Don't start planning what to say next.
* Show that you're listening.
* Don't impose your opinions or solutions.
* Stay focused.

# 2. Reflective Listening
### Question 2
#### According to Fisher's model, what are the key points of Reflective Listening? 

Listening is following the thoughts and feelings of another and understanding what the other is
saying from his or her perspective. Reflective listening is a special type of listening that involves
paying respectful attention to the content and feeling expressed in another persons’
communication. Reflective listening is hearing and understanding, and then letting the other
know that he or she is being heard and understood. It requires responding actively to another
while keeping your attention focused completely on the speaker. 

* It lets the speaker know that she or he has been heard, understood, card for, and supported.
* It gives the other feedback on what he or she said and how it came across.
* It allows you to check your own accuracy in hearing what the other has said.
* It avoids the illusion of understanding.
* It helps the other to think and articulate more clearly.
* It helps the other arrive at a solution to his or her own problem.
* It helps you clarify what you are expected to do.

# 3. Reflection
### Question 3
#### What are the obstacles in your listening process?

###### External Listening Barriers:
External listening barriers are easier to manage than internal barriers. They include a variety of environmental distractions that can usually be avoided or minimized with simple corrections, like removing yourself from the interfering barrier or removing the issue from the area that you are in. External barriers include:

* <b>Noise.</b> Any external noise can be a barrier, like the sound of equipment running, phones ringing, or other people having conversations.
* <b>Visual distractions.</b> Visual distractions can be as simple as the scene outside a window or the goings-on just beyond the glass walls of a nearby office.
* <b>Physical setting.</b> An uncomfortable temperature, poor or nonexistent seating, bad odors, or distance between the listener and speaker can be an issue.
* <b>Objects.</b> Items like pocket change, pens, and jewelry are often fidgeted with while listening.
* <b>The person speaking.</b> The person listening may become distracted by the other person’s personal appearance, mannerisms, voice, or gestures.

###### Internal Listening Barriers:
Internal listening barriers are more difficult to manage, as they reside inside the mind of the listener. Internal barriers’ elimination relies on a high level of self-awareness and discipline on the part of the listener, like catching oneself before the mind starts to wander and bringing full attention back to the speaker. Internal barriers include:

* <b>Anxiety.</b> Anxiety can take place from competing personal worries and concerns.
* <b>Self-centeredness.</b> This causes the listener to focus on his or her own thoughts rather than the speaker’s words.
* <b>Mental laziness.</b> Laziness creates an unwillingness to listen to complex or detailed information.
* <b>Boredom.</b> Boredom stems from a lack of interest in the speaker’s subject matter.
* <b>Sense of superiority.</b> This leads the listener to believe they have nothing to learn from the speaker.
* <b>Cognitive dissonance.</b> The listener hears only what he or she expects or molds the speaker’s message to conform with their own beliefs.
* <b>Impatience.</b> A listener can become impatient with a speaker who talks slowly or draws out the message.

### Question 4
#### What can you do to improve your listening?

* Consider eye contact.
* Be alert, but not intense.
* Pay attention to nonverbal signs, such as body language and tone.
* Make a mental image of what the speaker is saying.
* Empathise with the speaker.
* Provide feedback.
* Keep an open mind.

# 4. Types of Communication
### Question 5
#### When do you switch to Passive communication style in your day to day life?
passive communication is an ineffective way of getting one’s views across. Passive communicators rarely express their needs, thoughts or feelings in public. Instead, they hide their emotions and let others ‘walk over them’. While a passive communication style may seem harmless, it’s not. Because it makes people believe that such communicators are powerless, pushovers or uninterested, when that may be far from the truth.
And in my case I use passive communication when I did not like something and want to tell the other person without offending him. Then i use the passive communuication.

### Question 6
#### When do you switch into Aggressive communication styles in your day to day life?
When someone annoys me during my work. And keep poking me with some nonsense question or something.

### Question 7
#### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Generally, I switch to passive agressive/ sarcasm when I am with my friends.

### Question 8
#### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

* Choose the right time, one should choose the right time to bring up an issue.
* Be direct. Be direct in your conversation, do not beat around the bush.
* Say “I,” not “we.”
* Use body language to emphasize your words.
* Stand up for yourself. If you are not wrong, do not doubt yourself and beat yourself down.





















