# Learning process

# 1. How to Learn Faster with the Feynman Technique

## Question 1
### What is the Feynman Technique? <br>
The Feynman Technique is a method of learning that unleashes your potential and forces you to develop a deep understanding.

## Question 2
### What are the different ways to implement this technique in your learning process? <br>

* <b>Choose a concept to learn.</b> <br>Select a topic you’re interested in learning about and write it at the top of a blank page in a notebook.  
* <b>Teach it to yourself or someone else.</b> <br> Write everything you know about a topic out as if you were explaining it to yourself. Alternately, actually teach it to someone else.
* <b>Return to the source material if you get stuck.</b> <br> Go back to whatever you’re learning from – a book, lecture notes, podcast – and fill the gaps in your knowledge.
* <b>Simplify your explanations and create analogies.</b> <br> Streamline your notes and explanation, further clarifying the topic until it seems obvious. Additionally, think of analogies that feel intuitive.

# 2. Learning How to Learn TED talk by Barbara Oakley
## Question 3
### Paraphrase the video in detail in your own words.
The video talks about how we can unlearn the things and restructure or learning to make the learning process more efficient, which are listed below:-

For an example take one problem or something which you think that should be done and try to solve the problem after giving sometime to the problem try to take a rest and you will see the your passive brain will think the new ways

Try to play on your strengths, that is if you take more time to understand something, don't think of this as your weakness but rather understand that you are learning whatever you are learning at a much deeper level.

## Question 4
### What are some of the steps that you can take to improve your learning process?
* When you are learning something take a break in between, this let the brain passively think on the topic

* Once einstein said "If you can not explain something to a kid that means you do not know that very well".
 Try to teach the topic that you are trying to learn in simple and plain language

* Identify the problem and review the resources to correct the problem.

# 3. Learn Anything in 20 hours
## Question 5
### Your key takeaways from the video?
The video talks about how one should approach learning a new skill. The video expalins the point that one doesn't need 10,000 hours to learn a some new skills it just needs 20 hours to learn enough that one can work with the skill.

## Question 6
### What are some of the steps that you can while approaching a new topic?
<b>Some of the steps that can be implemented while adopting a new skill are:-</b>
* Learn the skill and prioritise the important topics first.
* Learn enough to self-correct your ownself, learn enough about something that you will be able to contemplate about that skill by your own.
* Remove practice barriers, that is do not judge yourself while learning a new skill if you get underconfident, it becomes harder to learn something.
* Learn something at least for 20 hours.



