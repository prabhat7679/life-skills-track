# Energy Management

## 1. Manage Energy not Time

#### Question 1:
##### What are the activities you do that make you relax - Calm quadrant?
* Playing video games.
* Listening to songs.
* Having a conversation with friends.

#### Question 2:
##### When do you find getting into the Stress quadrant?
* When i am working for a long period of time without having proper breaks or working under pressure.

#### Question 3:
##### How do you understand if you are in the Excitement quadrant?
* when i completed the assigned tasks in time.

## 2. Strategies to deal with Stress

#### Question 4:
##### Paraphrase the Sleep is your Superpower video in detail.
* Sleep deprivation can reduce your learning capacity by 40%, as it stops the hippocampus  your brain to receives any signals.
* It has been found that sleep deprivation also causes an increased risk of cardiovascular diseases.
* A study showed that lack of sleep also negatively affects your immune system.

#### Question 5:
##### What are some ideas that you can implement to sleep better?
* Having a good sleep.
* while sleeping listening to music.
* Maintain a temperature of 18 degrees Celcius in your room. It provides a comfortable temperature for your body.

#### Question 6:
##### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* Exercising can increase focus span.
* Exercising makes your pre-frontal cortex more immune to cognitive diseases.
* Studies have shown that exercising can increase the volume of the hippocampus.
* Memory gets better by exercising.

#### Question 7
##### What are some steps you can take to exercise more?
* Use of stairs, whenever possible
* Walking to the destination instead of taking a help vehicle






