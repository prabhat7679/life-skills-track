# Caching

## What is Caching?
In computing, a cache is a high-speed data storage layer which stores a subset of data, typically transient in nature, so that future  requests for that data are served up faster than is possible by accessing the data’s primary storage location. Caching allows you to      efficiently reuse previously retrieved or computed data.

## How does Caching work?
The data in a cache is generally stored in fast access hardware such as RAM (Random-access memory) and may also be used in correlation with a software component. A cache's primary purpose is to increase data retrieval performance by reducing the need to access the underlying slower storage layer.

Trading off capacity for speed, a cache typically stores a subset of data transiently, in contrast to databases whose data is usually complete and durable.

## Caching Overview
####  RAM and In-Memory Engines: 
Due to the high request rates or IOPS (Input/Output operations per second) supported by RAM and In-Memory engines, caching results in improved data retrieval performance and reduces cost at scale.

####  Applications: 
Caches can be applied and leveraged throughout various layers of technology including Operating Systems, Networking layers including Content Delivery Networks (CDN) and DNS, web applications, and Databases. You can use caching to significantly reduce latency and improve IOPS for many read-heavy application workloads, such as Q&A portals, gaming, media sharing, and social networking.

####  Design Patterns: 
In a distributed computing environment, a dedicated caching layer enables systems and applications to run independently from the cache with their own lifecycles without the risk of affecting the cache. The cache serves as a central layer that can be accessed from disparate systems with its own lifecycle and architectural topology. This is especially relevant in a system where application nodes can be dynamically scaled in and out.

####  Caching Best Practices: 
When implementing a cache layer, it’s important to understand the validity of the data being cached. A successful cache results in a high hit rate which means the data was present when fetched. A cache miss occurs when the data fetched was not present in the cache. Controls such as TTLs (Time to live) can be applied to expire the data accordingly. 

## Performance caching Approach
####  Server-Side Caching
1) Remote caches : 
A remote cache (or side cache) is a separate instance (or separate instances) dedicated for storing the cached data in-memory.
Remote caches are stored on dedicated servers and are typically built on key/value NoSQL stores, such as Redis and Memcached.
with remote caches, the orchestration between caching the data and managing the validity of the data is managed by your applications and/or processes that use it.

 * In Reactive approach, cache is updated after the data is requested.
 * In Proactive approach, cache is updated immediately when the primary database is updated.
 
 ####   Client Side Caching

 1) Browser level :
 Images, HTML code, stylesheets and Javascript libraries.

 2) Network request/response :
 Put api requests-response as key value pair in browser or app local storage.
 Use libraries or frameworks for having such local cache.

## Scaling caching Approach

####  Database Caching
Caching of data from the database is the primary use for caches in a cloud native architecture. Required data is first checked for in the cache. If it is available, the data is used immediately, without requiring direct database access. If not, then the data is retrieved from the database and stored into the cache for later retrieval.

####  Server Page Caching
For application-generated pages, caching speeds up response time greatly. After a server-side page has been rendered, the application may choose to cache the entire page into a caching server and return it when the page is requested once again.

####  Server Fragment Caching
The difficulty with server page caching is that it doesn’t always fit for your application needs, due to per-user customization requirements. Instead, servers should cache fragments of a page. This reduces the time required to fetch the associated data, run any application logic, and generate the server-side code.


# Resources
- [Caching](https://aws.amazon.com/caching/)
- [Performance caching Approach](https://arunrajeevan.medium.com/how-to-improve-performance-through-caching-4dd2799f9e7b)
- [Scaling caching Approach](https://realscale.cloud66.com/caching-strategies-for-scaling/)